package hu.renes.currencytest.view.main;

import android.util.Log;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import hu.renes.currencytest.R;
import hu.renes.currencytest.domain.interactor.ResourceInteractor;
import hu.renes.currencytest.domain.interactor.revolut.CurrencyInteractor;
import hu.renes.currencytest.domain.network.data.CountryCode;
import hu.renes.currencytest.domain.network.data.CurrencyList;
import hu.renes.currencytest.view.main.adapter.CurrencyVM;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import solid.collectors.ToList;
import solid.functions.Action1;
import solid.stream.Stream;

import static java.util.concurrent.TimeUnit.SECONDS;

public class MainActivityPresenter extends MvpBasePresenter<MainActivityView> {
    private static final String TAG = MainActivityPresenter.class.getCanonicalName();

    private static final int TIMER_ONE_SEC = 1;

    private final CurrencyInteractor currencyInteractor;
    private final ResourceInteractor resourceInteractor;
    private CompositeDisposable disposables;
    private double amount = 10.0;
    private CountryCode countryCode = CountryCode.EUR;
    List<CurrencyVM> currencyList = new ArrayList<>();

    @Inject
    MainActivityPresenter(final CurrencyInteractor currencyInteractor,
                          final ResourceInteractor resourceInteractor) {
        this.currencyInteractor = currencyInteractor;
        this.resourceInteractor = resourceInteractor;
    }

    void amountChanged(final double amount) {
        Log.d(TAG, "Amount changed");
        this.amount = amount;
        Stream.stream(currencyList).forEach((Action1<? super CurrencyVM>) currencyVM -> currencyVM.setAmount(amount));
        ifViewAttached(view -> view.onCurrenciesReady(currencyList));
    }

    void itemSelected(final CountryCode countryCode, final double amount) {
        Log.d(TAG, "Item has been selected");
        this.countryCode = countryCode;
        this.amount = amount;
    }

    void loadCurrencies() {
        Log.d(TAG, "Loading rates: " + countryCode.name());
        disposables.clear();
        disposables.add(currencyInteractor.getCurrencyList(countryCode)
                .delay(TIMER_ONE_SEC, SECONDS)
                .repeat()
                .map(currencyList -> currencyDTOList(currencyList, countryCode, amount))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(response -> {
                            if (currencyList.size() == 0) {
                                this.currencyList.addAll(response);
                            } else {
                                for (CurrencyVM currency : currencyList) {
                                    CurrencyVM item = Stream.stream(response)
                                            .filter(value -> value.getCountryCode() == currency.getCountryCode())
                                            .first().get();
                                    currency.setCurrencyValue(item.currencyValue);
                                    currency.setAmount(amount);
                                }
                            }
                            ifViewAttached(view -> view.onCurrenciesReady(currencyList));
                        },
                        throwable -> ifViewAttached(view -> view.onErrorOccured(resourceInteractor.getStringResource(R.string.main_network_error)))));
    }

    private List<CurrencyVM> currencyDTOList(final CurrencyList currencyList, final CountryCode countryCode, final double amount) {
        final List<CurrencyVM> currencyDTOList = Stream.stream(currencyList.rates.entrySet())
                .map(e -> new CurrencyVM(e.getKey(), e.getValue(), amount))
                .collect(ToList.toList());
        currencyDTOList.add(0, new CurrencyVM(countryCode, 1.0, amount));
        return currencyDTOList;
    }


    @Override
    public void attachView(@NotNull final MainActivityView view) {
        super.attachView(view);
        disposables = new CompositeDisposable();
    }

    @Override
    public void detachView() {
        if (disposables != null && !disposables.isDisposed()) {
            disposables.dispose();
        }

        super.detachView();
    }
}
