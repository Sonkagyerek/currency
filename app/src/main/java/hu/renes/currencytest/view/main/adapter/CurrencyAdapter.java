package hu.renes.currencytest.view.main.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import javax.inject.Inject;

import hu.renes.currencytest.injection.qualifier.ApplicationContext;
import hu.renes.currencytest.view.base.adapter.BaseAdapter;
import solid.stream.Stream;

public class CurrencyAdapter extends BaseAdapter<CurrencyVM> {

    private CurrencyViewHolder.OnItemClickListener itemClickListener;
    private CurrencyViewHolder.OnValueListener valueChangeListener;

    @Inject
    public CurrencyAdapter(@ApplicationContext Context context) {
        super(context);
    }

    @NotNull
    @Override
    public CurrencyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        final CurrencyViewHolder viewHolder = new CurrencyViewHolder(view, context);
        viewHolder.setOnItemClickListener(itemClickListener);
        viewHolder.setOnValueListener(valueChangeListener);
        return viewHolder;
    }

    public void setItemClickListener(CurrencyViewHolder.OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void setValueChangeListener(CurrencyViewHolder.OnValueListener valueChangeListener) {
        this.valueChangeListener = valueChangeListener;
    }
}
