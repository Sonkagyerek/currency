package hu.renes.currencytest.view.base.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * Base ViewHolder for list's viewholders
 * Registers the item clicks
 */
public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {

    protected final Context context;
    private View clickableView; // The view which can be clicked on the list

    /**
     * List item click listener interface
     */
    public interface OnItemClickListener {

        /**
         * A list item is clicked on the list
         *
         * @param v        The parent view is clicked
         * @param position The clicked item position
         */
        void listItemClicked(View v, int position);
    }

    public BaseViewHolder(View view, Context context) {
        super(view);
        setClickableView(view);
        this.context = context;
    }

    public void setClickableView(View view) {
        this.clickableView = view;
    }

    public View getClickableView() {
        return clickableView;
    }

    abstract public void bind(T data);
}
