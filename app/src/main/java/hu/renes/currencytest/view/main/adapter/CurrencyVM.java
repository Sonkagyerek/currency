package hu.renes.currencytest.view.main.adapter;

import hu.renes.currencytest.R;
import hu.renes.currencytest.domain.network.data.CountryCode;
import hu.renes.currencytest.view.base.adapter.IListTypeProvider;

public class CurrencyVM implements IListTypeProvider {

    private static final String FLAG_URL = "https://www.countryflags.io/%s/flat/64.png";

    public String shortName;
    public String longName;
    public Double currencyValue;
    public String flagPath;
    public double amount;
    private CountryCode code;


    public CurrencyVM(final CountryCode code, final double currencyValue, final double amount) {
        this.code = code;
        this.shortName = code.name().toUpperCase();
        this.longName = code.getCurrency();
        this.currencyValue = currencyValue;
        this.amount = amount;
        this.flagPath = String.format(FLAG_URL, code.name().substring(0, 2));
    }

    public CountryCode getCountryCode() {
        return code;
    }

    public void setAmount(final double amount) {
        this.amount = amount;
    }

    public void setCurrencyValue(final double currencyValue) {
        this.currencyValue = currencyValue;
    }

    @Override
    public int getLayoutType() {
        return R.layout.item_currency;
    }
}
