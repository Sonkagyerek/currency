package hu.renes.currencytest.view.main;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import hu.renes.currencytest.R;
import hu.renes.currencytest.injection.component.ActivityComponent;
import hu.renes.currencytest.utility.DialogHelper;
import hu.renes.currencytest.view.base.BaseActivity;
import hu.renes.currencytest.view.main.adapter.CurrencyAdapter;
import hu.renes.currencytest.view.main.adapter.CurrencyVM;

public class MainActivity extends BaseActivity<MainActivityView, MainActivityPresenter> implements MainActivityView {

    @Inject
    MainActivityPresenter presenter;
    @Inject
    DialogHelper dialogHelper;
    @Inject
    CurrencyAdapter adapter;

    @BindView(R.id.recycleView)
    RecyclerView recyclerView;
    @BindView(R.id.progressView)
    ProgressBar loadingProgressBar;

    @Override
    protected void injectActivity(final ActivityComponent activityComponent) {
        getActivityComponent().inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initAdapter();
        presenter.loadCurrencies();
    }

    @NonNull
    @Override
    public MainActivityPresenter createPresenter() {
        return presenter;
    }

    @Override
    public void onErrorOccured(final String message) {
        loadingProgressBar.setVisibility(View.GONE);
        dialogHelper.createDialog(getString(R.string.general_warning), message)
                .show();
    }

    @Override
    public void onCurrenciesReady(final List<CurrencyVM> currencyVMList) {
        loadingProgressBar.setVisibility(View.GONE);
        if (!recyclerView.isComputingLayout()) {
            adapter.setItems(currencyVMList);
        }
    }

    private void initAdapter() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, layoutManager.getOrientation()));
        adapter.setValueChangeListener(value -> presenter.amountChanged(value));
        adapter.setItemClickListener((currencyVM, value) -> {
            presenter.itemSelected(currencyVM.getCountryCode(), value);
            final int currentPosition = adapter.getItemPosition(currencyVM);
            if (currentPosition > 0 && !recyclerView.isComputingLayout()) {
                presenter.currencyList.remove(currentPosition);
                presenter.currencyList.add(0, currencyVM);
                adapter.notifyItemMoved(currentPosition, 0);
                layoutManager.scrollToPosition(0);
                presenter.loadCurrencies();
            }
        });
    }
}
