package hu.renes.currencytest.view.main;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import hu.renes.currencytest.view.main.adapter.CurrencyVM;

public interface MainActivityView extends MvpView {

    void onErrorOccured(final String message);

    void onCurrenciesReady(final List<CurrencyVM> currencyVMList);
}
