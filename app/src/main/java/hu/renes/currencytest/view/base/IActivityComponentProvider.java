package hu.renes.currencytest.view.base;

import hu.renes.currencytest.injection.component.ActivityComponent;

public interface IActivityComponentProvider {
    ActivityComponent getActivityComponent();
}
