package hu.renes.currencytest.view.base;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import hu.renes.currencytest.CurrencyApplication;
import hu.renes.currencytest.injection.component.ActivityComponent;
import hu.renes.currencytest.injection.component.DaggerActivityComponent;
import hu.renes.currencytest.injection.module.ActivityModule;

public abstract class BaseActivity<V extends MvpView, P extends MvpPresenter<V>> extends MvpActivity<V, P> implements IActivityComponentProvider {

    private ActivityComponent activityComponent;

    protected abstract void injectActivity(ActivityComponent activityComponent);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        injectActivity(getActivityComponent());
        super.onCreate(savedInstanceState);
    }

    public ActivityComponent getActivityComponent() {
        if (activityComponent == null) {
            activityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .applicationComponent(CurrencyApplication.get(this).getApplicationComponent())
                    .build();
        }
        return activityComponent;
    }
}