package hu.renes.currencytest.view.base.adapter;

public interface IListTypeProvider {
    /**
     * @return the layout identifier as type
     */
    int getLayoutType();
}
