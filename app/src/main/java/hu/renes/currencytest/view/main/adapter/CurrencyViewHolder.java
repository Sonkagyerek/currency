package hu.renes.currencytest.view.main.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import hu.renes.currencytest.R;
import hu.renes.currencytest.view.base.adapter.BaseViewHolder;

public class CurrencyViewHolder extends BaseViewHolder<CurrencyVM> {

    private static final int FLAG_SIZE = 220;


    @BindView(R.id.flagImage)
    ImageView flagImageView;
    @BindView(R.id.currency)
    TextView shortNameTextView;
    @BindView(R.id.symbol)
    TextView symbolTextView;
    @BindView(R.id.value)
    EditText valueEditText;

    private OnItemClickListener onItemClickListener;
    private OnValueListener onValueListener;

    public CurrencyViewHolder(final View view, final Context context) {
        super(view, context);
        ButterKnife.bind(this, view);
    }

    @Override
    public void bind(CurrencyVM data) {
        if (data == null) {
            return;
        }
        shortNameTextView.setText(data.shortName);
        symbolTextView.setText(data.longName);
        Picasso.get()
                .load(data.flagPath)
                .placeholder(R.drawable.ic_load)
                .resize(FLAG_SIZE, FLAG_SIZE)
                .into(flagImageView);

        final double rate = data.currencyValue * data.amount;
        if (!valueEditText.isFocused()) {
            valueEditText.setText(String.format(Locale.US, "%.2f", rate));
        }

        itemView.setOnClickListener(view -> focusOnItem(data, rate));

        valueEditText.setSelection(valueEditText.getSelectionStart(), valueEditText.getSelectionEnd());
        valueEditText.setOnFocusChangeListener((view, focus) -> {
            if (focus) {
                focusOnItem(data, rate);
            }
        });
        valueEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(final CharSequence charSequence, final int i, final int i1, final int i2) {

            }

            @Override
            public void onTextChanged(final CharSequence charSequence, final int i, final int i1, final int i2) {
                if (valueEditText.getText() != null && !valueEditText.getText().toString().isEmpty() && valueEditText.isFocused()) {
                    onValueListener.onValueChanged(Double.parseDouble(valueEditText.getText().toString()));
                    valueEditText.clearFocus();
                }
            }

            @Override
            public void afterTextChanged(final Editable editable) {

            }
        });
    }

    private void focusOnItem(final CurrencyVM data, final double value) {
        onItemClickListener.listItemClicked(data, value);
    }

    public void setOnValueListener(final OnValueListener listener) {
        this.onValueListener = listener;
    }

    public void setOnItemClickListener(final OnItemClickListener clickListener) {
        this.onItemClickListener = clickListener;
    }

    public interface OnItemClickListener {
        void listItemClicked(CurrencyVM currencyVM, double value);
    }

    public interface OnValueListener {
        void onValueChanged(double value);
    }
}
