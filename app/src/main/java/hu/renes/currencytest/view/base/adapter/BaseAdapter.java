package hu.renes.currencytest.view.base.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("unused")
public abstract class BaseAdapter<T extends IListTypeProvider> extends RecyclerView.Adapter<BaseViewHolder<T>> implements View.OnClickListener {

    protected final Context context;
    private List<T> items;
    private BaseViewHolder.OnItemClickListener onClickListener;

    /**
     * Default constructor
     */
    public BaseAdapter(Context context) {
        this.context = context;
        items = new ArrayList<>();
    }

    /**
     * Constructor with pre initialized list
     *
     * @param context
     * @param items   The new items
     */
    public BaseAdapter(Context context, @NonNull List<T> items) {
        this.context = context;
        this.items = items;
    }

    /**
     * Costructor with pre initialized list and onclicklistener
     *
     * @param context
     * @param items           The new items
     * @param onClickListener Item onclicklistenr
     */
    public BaseAdapter(Context context, @NonNull List<T> items, BaseViewHolder.OnItemClickListener onClickListener) {
        this.context = context;
        this.items = items;
        this.onClickListener = onClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getLayoutType();
    }

    @Override
    public void onBindViewHolder(BaseViewHolder<T> holder, int position) {
        holder.bind(getItem(position));
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    /**
     * Get item from the list via position
     *
     * @param position The item's position
     * @return
     */
    public T getItem(int position) {
        return items == null ? null : items.get(position);
    }

    /**
     * Get the position of the given item
     *
     * @param item The item
     * @return The position of the item if is in the collection, -1 otherwise
     */
    public int getItemPosition(T item) {
        if (items.contains(item)) {
            return items.indexOf(item);
        } else {
            return -1;
        }
    }

    /**
     * Get all items
     *
     * @return
     */
    public List<T> getItems() {
        return items == null ? Collections.<T>emptyList() : items;
    }

    /**
     * Set a new set of items
     *
     * @param items The new items
     */
    public void setItems(List<T> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    /**
     * Add new item to the end of the list
     *
     * @param item The new item
     */
    public void addItem(T item) {
        items.add(item);
        notifyItemInserted(items.size() - 1);
    }

    /**
     * Add new item to the list to the specified position
     *
     * @param position The position to insert
     * @param item     The new item
     */
    public void addItem(int position, T item) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    /**
     * Add a list to the end of the existing list
     *
     * @param items The new list of items
     */
    public void addItems(List<T> items) {
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    /**
     * Remove the given item from the list
     *
     * @param item The removable item
     */
    public void removeItem(T item) {
        if (items.contains(item)) {
            int position = items.indexOf(item);
            items.remove(item);
            notifyItemRemoved(position);
        }
    }

    /**
     * Remove item via it's position in the list
     *
     * @param position The item's position
     */
    public void removeItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    /**
     * Remove all items from the list
     */
    public void removeAllItems() {
        int size = items.size();
        items.clear();
        notifyItemRangeRemoved(0, size);
    }

    /**
     * Update a single item in the list
     *
     * @param oldItem the item you want to update
     * @param newItem the item you want to insert in place of the old one
     */
    public void updateItem(T oldItem, T newItem) {
        if (items.contains(oldItem)) {
            int position = items.indexOf(oldItem);
            items.set(position, newItem);
            notifyItemChanged(position);
        }
    }

    /**
     * Notify item is changed in the list
     *
     * @param item The changed item
     */
    public void notifyItemChanged(T item) {
        if (items.contains(item)) {
            notifyItemChanged(items.indexOf(item));
        }
    }

    /**
     * Prepares the selected view for handling the tap if it was set before
     *
     * @param vh The actual viewholder
     */
    protected void prepareItemOnClick(BaseViewHolder vh) {
        View view = vh.getClickableView();
        if (view != null) {
            view.setOnClickListener(this);
            view.setTag(vh);
        }
    }

    @Override
    public void onClick(View v) {
        if (onClickListener != null) {
            BaseViewHolder vh = (BaseViewHolder) v.getTag();
            onClickListener.listItemClicked(v, vh.getPosition());
        }
    }

    public BaseViewHolder.OnItemClickListener getOnClickListener() {
        return onClickListener;
    }

    public void setOnClickListener(BaseViewHolder.OnItemClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public Context getContext() {
        return context;
    }
}
