package hu.renes.currencytest.domain.network.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

public class CurrencyList {
    @SerializedName("base")
    public String base;
    @SerializedName("date")
    public String date;
    @SerializedName("rates")
    public Map<CountryCode, Double> rates;
}
