package hu.renes.currencytest.domain.network.service;

import hu.renes.currencytest.domain.network.data.CurrencyList;
import io.reactivex.Single;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RevolutService {

    @GET("/latest")
    Single<CurrencyList> currencies(@Query("base") String base);

    class Factory {
        public static RevolutService createService(final Retrofit retrofit) {
            return retrofit.create(RevolutService.class);
        }
    }
}
