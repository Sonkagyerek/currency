package hu.renes.currencytest.domain.interactor.revolut;


import javax.inject.Inject;

import hu.renes.currencytest.domain.network.data.CountryCode;
import hu.renes.currencytest.domain.network.data.CurrencyList;
import hu.renes.currencytest.domain.network.service.RevolutService;
import io.reactivex.Single;

public class CurrencyInteractor {

    private final RevolutService revolutService;

    @Inject
    public CurrencyInteractor(final RevolutService revolutService) {
        this.revolutService = revolutService;
    }

    public Single<CurrencyList> getCurrencyList(final CountryCode code) {
        return revolutService.currencies(code.name());
    }
}
