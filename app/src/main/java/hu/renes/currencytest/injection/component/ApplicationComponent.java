package hu.renes.currencytest.injection.component;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import hu.renes.currencytest.CurrencyApplication;
import hu.renes.currencytest.domain.interactor.ResourceInteractor;
import hu.renes.currencytest.domain.network.service.RevolutService;
import hu.renes.currencytest.injection.module.ApplicationModule;
import hu.renes.currencytest.injection.module.NetworkModule;
import hu.renes.currencytest.injection.qualifier.ApplicationContext;
import hu.renes.currencytest.utility.DialogHelper;


@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent {

    void inject(CurrencyApplication application);

    ResourceInteractor resourceInteractor();

    @ApplicationContext
    Context context();

    RevolutService revolutService();
}
