package hu.renes.currencytest.injection.module;

import android.app.Application;
import android.content.Context;

import dagger.Module;
import dagger.Provides;
import hu.renes.currencytest.injection.qualifier.ApplicationContext;

@Module
public class ApplicationModule {
    protected Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    public Application provideApplication() {
        return application;
    }

    @ApplicationContext
    @Provides
    public Context provideContext() {
        return application.getApplicationContext();
    }
}
