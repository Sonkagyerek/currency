package hu.renes.currencytest.injection.component;

import android.content.Context;

import dagger.Component;
import hu.renes.currencytest.injection.module.ActivityModule;
import hu.renes.currencytest.injection.qualifier.ActivityContext;
import hu.renes.currencytest.injection.scope.ActivityScope;
import hu.renes.currencytest.utility.DialogHelper;
import hu.renes.currencytest.view.main.MainActivity;


@ActivityScope
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity activity);

    // Activities injections should be come here
    @ActivityContext
    Context context();

    DialogHelper dialogHelper();
}
