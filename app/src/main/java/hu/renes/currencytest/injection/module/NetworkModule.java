package hu.renes.currencytest.injection.module;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.renes.currencytest.BuildConfig;
import hu.renes.currencytest.domain.network.service.RevolutService;
import hu.renes.currencytest.injection.qualifier.ApplicationContext;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static java.util.concurrent.TimeUnit.SECONDS;
import static okhttp3.logging.HttpLoggingInterceptor.Level.BODY;

@Module
public class NetworkModule {

    private static final int TIMEOUT_IN_SEC = 30;

    @Provides
    @Singleton
    Retrofit provideWebSzigno(final Gson gson, final OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    Picasso providePicasso(@ApplicationContext Context context, final OkHttpClient okHttpClient) {
        return new Picasso.Builder(context)
                .downloader(new OkHttp3Downloader(okHttpClient))
                .loggingEnabled(BuildConfig.DEBUG)
                .indicatorsEnabled(false)
                .build();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(final HttpLoggingInterceptor logger) {

        return new OkHttpClient.Builder()
                .addInterceptor(logger)
                .connectTimeout(TIMEOUT_IN_SEC, SECONDS)
                .readTimeout(TIMEOUT_IN_SEC, SECONDS)
                .build();
    }


    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().create();
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor provideHttpLogger() {
        final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            logging.level(BODY);
        }
        return logging;
    }

    @Provides
    @Singleton
    public RevolutService providePaymentService(Retrofit retrofit) {
        return RevolutService.Factory.createService(retrofit);
    }
}
