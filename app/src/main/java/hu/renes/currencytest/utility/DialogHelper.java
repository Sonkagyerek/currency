package hu.renes.currencytest.utility;

import android.content.Context;

import androidx.appcompat.app.AlertDialog;

import javax.inject.Inject;

import hu.renes.currencytest.injection.qualifier.ActivityContext;

public class DialogHelper {

    private final Context context;

    @Inject
    public DialogHelper(@ActivityContext final Context context) {
        this.context = context;
    }

    public AlertDialog createDialog(final String title, final String message) {
        return new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, (dialog, i) -> dialog.dismiss())
                .create();
    }
}
