package hu.renes.currencytest;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.LifecycleObserver;

import com.squareup.picasso.Picasso;

import hu.renes.currencytest.injection.component.ApplicationComponent;
import hu.renes.currencytest.injection.component.DaggerApplicationComponent;
import hu.renes.currencytest.injection.module.ApplicationModule;
import hu.renes.currencytest.injection.module.NetworkModule;

public class CurrencyApplication extends Application implements LifecycleObserver {

    private ApplicationComponent applicationComponent;
    private static CurrencyApplication application;

    @Override
    public void onCreate() {
        super.onCreate();

        application = this;

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .networkModule(new NetworkModule())
                .build();
        applicationComponent.inject(this);

        final Picasso picasso = new Picasso.Builder(this)
                .indicatorsEnabled(false)
                .build();
        Picasso.setSingletonInstance(picasso);
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    public static CurrencyApplication get(Context context) {
        return (CurrencyApplication) context.getApplicationContext();
    }

    public static CurrencyApplication getApplication() {
        return application;
    }
}
